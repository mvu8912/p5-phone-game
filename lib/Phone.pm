package Phone;

use Moose;

has number => (
    is       => "ro",
    isa      => "Int",
    required => 1,
);

=head1 NAME

Phone - A basic phone class

=head1 SYNOPSIS

 use Phone;

 my $amy = Phone->new(number => "123");

 my $emma = Phone->new(number => "456");

 $amy->call($emma->number);

 $emma->receive_call($amy->number);

 my $message = "Hello";

 $emma->sms($amy->number, $message);

 $amy->receive_sms($emma->number, $message);

=head1 DESCRIPTION

This is a very basic phone class. It can make call / receive call.

It can send / receive SMS.

=head1 METHODS

=head2 call

Calling a number

 $phone->call( $outgoing_phone_number );

=cut
sub call {
    my $self   = shift;
    my $number = shift;
    return "calling to $number ...\n";
}

=head2 receive_call

Receive a call
    
 $phone->receive_call( $incoming_caller_number )

=cut

sub receive_call {
    my $self   = shift;
    my $number = shift;
    return "calling from $number ...\n";
}

=head2 sms

Sending a text message

 $phone->sms( $outgoing_phone_number, $your_text_message );

=cut

sub sms {
    my $self    = shift;
    my $number  = shift;
    my $message = shift;
    return "sending message to $number\nmessage: $message\n";
}

=head2 receive_sms

Receive a text message

 $phone->receive_sms( $incoming_messenger_number, $their_text_message );

=cut

sub receive_sms {
    my $self    = shift;
    my $number  = shift;
    my $message = shift;
    return "receiving message from $number\nmessage: $message\n";
}

=head1 AUTHOR

"Michael Vu" <email@michael.vu>

=head1 COPYRIGHT

Copyright 2013-2006 by Michael Vu <email@michael.vu>

This program is only for showing coding styles purpose.

Not for selling or redistribute and/or modify it under the author's authorisation.

=cut

1;
