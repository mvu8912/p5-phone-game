#!/usr/bin/perl 
use strict;
use warnings;
use Phone;
use Data::UUID;
use IO::Prompt;

## Friend Name
my @friends = qw( bob amy emma tommy );
## Friend Number
my %phones = map { $_ => Phone->new( number => _phone_number() ) } @friends; 
## My number
my $my_phone = Phone->new( number => _phone_number() );

## Start calling game
start_game();

sub start_game {
    my $my_name = _get_name();
    print "Hi $my_name. This is your phone number " . $my_phone->number . ".\n";
    my $action = _menu();
    _execute($action)
}

## Run user's action "call" / "sms"
sub _execute {
    my $action = shift;
    my $friend = _load_contacts();
    my $message = $action eq "sms" ? prompt("Message: ") : "";
    print "You are " . $my_phone->$action( $friend->{phone}->number, $message );
    my $method = "receive_$action";
    print "$friend->{name} is " . $friend->{phone}->$method( $my_phone->number, $message );
}

## Get a unique random mobile phone number
sub _phone_number {
    my $ug = Data::UUID->new;
    my $uuid = $ug->create_str;
    $uuid =~s/\D//g;
    return "07" . substr $uuid, 0, 9;
}

## Get your name
sub _get_name {
    while (1) {
        my $name = prompt("What is your name? ");
        if ( "$name" ) {
            return $name;
        }
    }
}

## Print the main menu
sub _menu {

    print "-- MENU --\nc. Make Call\ns. Send Text\nX. Turn off the phone\n-- ---- --\n";

    while (1) {
        my $option = prompt("Please Choose> ");
        if ( $option =~/c/i ) {
            return "call";
        }
        if ( $option =~/s/i ) {
            return "sms";
        }
        if ( $option =~/[Xq]/ ) {
            exit;
        }
        print "\n ** Wrong option **\n";
    }
}

## Print the contact list
sub _load_contacts {
    print "-- CONTACT LIST --\n";
    my $count = 0;
    print join "\n", map { $count++ . ".> $_" } @friends;
    print "\n";
    while (1) {
        my $no = prompt("> ");
        if ( $no !~/^\d$/ ) {
            next;
        }
        my $name = $friends[$no];
        my $phone =  $phones{$name};
        if ( $phone ) {
            return {
                name => $name,
                phone => $phone,
            }
        }
        print "\n ** Wrong choise **\n";;
    }
}

## Author: Michael Vu
