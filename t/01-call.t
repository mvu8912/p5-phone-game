use strict;
use warnings;

use Test::More tests => 2;                      # last test to print

use Phone;

my $amy = Phone->new(number => "123");

my $emma = Phone->new(number => "456");

is $amy->call($emma->number), "calling to " . $emma->number . " ...\n", "amy calls emma";

is $emma->receive_call($amy->number), "calling from " . $amy->number ." ...\n", "emma got call from amy";

## Author: Michael V u
