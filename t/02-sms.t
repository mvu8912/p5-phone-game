use strict;
use warnings;

use Test::More tests => 2;                      # last test to print

use Phone;

my $amy = Phone->new(number => "123");

my $emma = Phone->new(number => "456");

my $message = "Hello";

is $emma->sms($amy->number, $message), "sending message to " . $amy->number . "\nmessage: $message\n", "emma texts amy";

is $amy->receive_sms($emma->number, $message), "receiving message from " . $emma->number . "\nmessage: $message\n", "amy got a text from emm";

## Author Michael VU
